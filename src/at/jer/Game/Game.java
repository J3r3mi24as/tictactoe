package at.jer.Game;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    private ArrayList<Field> gameField;
    private Scanner scanner = new Scanner(System.in);


    public Game() {
        gameField = new ArrayList<>();
        initGameField();
        initMessage();
        int counter = 0;
        while(true) {
            if(counter == 0) {
                counter = 1;
            }
            else if(counter == 1) {
                counter = 2;
            }
            else if(counter == 2) {
                counter = 1;
            }
            gameTurn(counter);
            if(checkForWinner()){
                break;
            }
        }
    }



    private void gameTurn(Integer player) {
        System.out.println("Enter your choice Player "+player+"\n");
        userInput(player);
        printGame();

    }

    private void printGame() {
        for(int i = 0; i < 3; i++) {
            String field1 = "";


            System.out.println("|" + getStringForStatus(0+3*i) + "|"+ getStringForStatus(1+3*i) + "|"+ getStringForStatus(2+3*i) + "|");
        }
    }

    private String getStringForStatus(int index) {

        switch (gameField.get(index).getStatus()) {
            case 0:
                return " ";
            case 1:
                return "x";
            case 2:
                return "o";
            default:
                return "";
        }
    }

    private Boolean checkForWinner() {
        if(checkRows() > 0) {
            System.out.println("Winner: Player "+checkRows());
            return true;
        }
        if(checkColumns() > 0) {
            System.out.println("Winner: Player "+checkColumns());
            return true;
        }
        if(checkDiagonal() > 0) {
            System.out.println("Winner: Player "+ checkDiagonal());
            return true;
        }
        return false;
    }


    private int checkColumns() {
        for(int i = 0; i < 3; i++) {
            if(gameField.get(i).getStatus() == gameField.get(i+3).getStatus()&& gameField.get(i+3).getStatus() == gameField.get(i+6).getStatus()) {
                if(gameField.get(i).getStatus() == 0) {
                    return 0;
                }
                return gameField.get(i).getStatus();
            }
        }
        return 0;
    }

    private int checkRows() {
        for(int i = 0; i < 3; i++) {
            if(gameField.get(0+3*i).getStatus() == gameField.get(1+3*i).getStatus()&& gameField.get(1+3*i).getStatus() == gameField.get(2+3*i).getStatus()) {
                if(gameField.get(0+3*i).getStatus() == 0) {
                    continue;
                }
                return gameField.get(0+3*i).getStatus();
            }
        }
        return 0;
    }

    private int checkDiagonal() {
            if (gameField.get(0).getStatus() == gameField.get(4).getStatus() && gameField.get(4).getStatus() == gameField.get(8).getStatus()) {
                if (gameField.get(0).getStatus() == 0) {
                    return 0;
                }
                return gameField.get(0).getStatus();
            }

        if (gameField.get(2).getStatus() == gameField.get(4).getStatus() && gameField.get(4).getStatus() == gameField.get(6).getStatus()) {
            if (gameField.get(2).getStatus() == 0) {
                return 0;
            }
            return gameField.get(2).getStatus();
        }


        return 0;
    }

    private void userInput(Integer player) {
        while(true) {
            String input = scanner.nextLine();
            if(input != ""){

                System.out.println("input in");
                String[] inputArray = input.split(",");
                int x = Integer.parseInt(inputArray[0]);
                int y = Integer.parseInt(inputArray[1]);
                if(x > 2 || y > 2 || x < 0 || y < 0) {
                    System.out.println("Input not valid try again\n");
                    continue;
                }
                Boolean falseInput = false;
                for (Field field: gameField) {

                    if(field.getxIndex() == x && field.getyIndex() == y){
                        if(field.getStatus() == 0){
                            field.setStatus(player);
                        }
                        else {
                            falseInput = true;

                        }

                    }
                }
                if(falseInput) {
                    System.out.println("Field is already used. Try again:\n");
                    continue;
                }
                break;
            }
        }

    }

    private void initMessage() {
        System.out.println("********************");
        System.out.println("Welcome to TicTacToe");
        System.out.println("*********************");
    }

    private void initGameField() {
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                gameField.add(new Field(j, i));
            }
        }
    }
}