package at.jer.Game;

public class Field {
    private Integer xIndex;
    private Integer yIndex;
    private Integer status;


    public Field(Integer xIndex, Integer yIndex) {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.status = 0;
    }

    public Integer getxIndex() {
        return xIndex;
    }

    public Integer getyIndex() {
        return yIndex;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
